import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Contractor } from '../../shared/interfaces/interfaces';
import { DictionariesService } from '../../shared/dictionaries.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-contractors-form',
  templateUrl: './contractors-form.component.html',
  styleUrls: ['./contractors-form.component.scss']
})
export class ContractorsFormComponent implements OnInit {
  form: FormGroup;
  creatSubscription: Subscription;

  @Output() onSubmit = new EventEmitter();

  constructor(private service: DictionariesService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, [
        Validators.required
      ]),
      fullName: new FormControl(null, [
        Validators.required
      ]),
      TIN: new FormControl(null, [
        Validators.required
      ]),
      IEC: new FormControl(null, [
        Validators.required
      ])
    })
  }
/**
 * Создание нового контрагента
 */
  submit() {
    const contractor: Contractor = {
      name: this.form.value.name,
      fullName: this.form.value.fullName,
      TIN: this.form.value.TIN,
      IEC: this.form.value.IEC
    }
    this.creatSubscription = this.service.createContractor(contractor).subscribe(
      () => {
        this.form.reset();
        this.onSubmit.emit();
        console.log(this.creatSubscription);
      }
    )
  }

}
