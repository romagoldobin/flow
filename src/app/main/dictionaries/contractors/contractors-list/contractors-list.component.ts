import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ContractorShort } from '../../shared/interfaces/interfaces';

@Component({
  selector: 'app-contractors-list',
  templateUrl: './contractors-list.component.html',
  styleUrls: ['./contractors-list.component.scss']
})
export class ContractorsListComponent implements OnInit {

  @Input() contractors: ContractorShort[] = [];
  @Output() editClicked = new EventEmitter();
  @Output() deleteClicked = new EventEmitter();

  constructor() { }

  ngOnInit(): void {

  }
}
