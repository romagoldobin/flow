import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ContractorShort } from '../../../shared/interfaces/interfaces';

@Component({
  selector: 'app-contractors-list-item',
  templateUrl: './contractors-list-item.component.html',
  styleUrls: ['./contractors-list-item.component.scss']
})
export class ContractorsListItemComponent implements OnInit {

  @Input() contractor: ContractorShort = undefined;

  @Output() onEdit = new EventEmitter<string>();
  @Output() onDelete = new EventEmitter<string>();

  constructor() {

   }

  ngOnInit(): void {
  }

  onEditClick(id: string) {
    this.onEdit.emit(id);
  }

  onDeleteClick(id: string) {
    this.onDelete.emit(id);
  }

}
