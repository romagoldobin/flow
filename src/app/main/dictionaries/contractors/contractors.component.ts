import { Component, OnInit, OnDestroy } from '@angular/core';
import { DictionariesService } from '../shared/dictionaries.service';
import { ContractorShort } from '../shared/interfaces/interfaces';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-contractors',
  templateUrl: './contractors.component.html',
  styleUrls: ['./contractors.component.scss']
})
export class ContractorsComponent implements OnInit, OnDestroy {

  contractors: ContractorShort[] = [];

  /**
   * Для отписки(от утечек памяти)
   */
  getContractorsSubscription: Subscription;
  getContractorByIdSubscr: Subscription;

  constructor(private service: DictionariesService) { }

  ngOnInit(): void {
    this.getContractors();
  }

  ngOnDestroy(): void {
    if (this.getContractorsSubscription) {
      this.getContractorsSubscription.unsubscribe()
    }
    if (this.getContractorByIdSubscr) {
      this.getContractorByIdSubscr.unsubscribe()
    }
  }

  /**
   * Получение списка контрагентов
   */
  getContractors() {
    this.getContractorsSubscription = this.service.getContractors().subscribe(
      (contractors) => {
        this.contractors = contractors
        console.log(this.contractors)
      }
    )
  }

  /**
   * Получение данных контрагента по id
   * @param id контрагента
   */
  getContractorById(id: string = null) {
    this.service.getContractorById(id).subscribe(
      (contractor) => {
        console.log(contractor);
      }
    )
  }
  /**
   * Удаление контрагента
   * @param id контрагента
   */
  deleteContractor(id: string) {
    this.service.removeContractor(id).subscribe(
      () => {
        this.getContractors();
      }
    )
  }

}
