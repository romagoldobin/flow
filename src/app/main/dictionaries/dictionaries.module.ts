import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DictionariesRoutingModule } from './dictionaries-routing.module';
import { DictionariesComponent } from './dictionaries.component';
import { ContractorsComponent } from './contractors/contractors.component';
import { ContractorsListComponent } from './contractors/contractors-list/contractors-list.component';
import { ContractorsFormComponent } from './contractors/contractors-form/contractors-form.component';
import { ContractorsListItemComponent } from './contractors/contractors-list/contractors-list-item/contractors-list-item.component';
import { PaymentTypeDocumentComponent } from './payment-type-document/payment-type-document.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticlesFormComponent } from './articles/articles-form/articles-form.component';
import { ArticlesListComponent } from './articles/articles-list/articles-list.component';
import { ArticlesListItemComponent } from './articles/articles-list/articles-list-item/articles-list-item.component';
import { PaymentTypeDocumentFormComponent } from './payment-type-document/payment-type-document-form/payment-type-document-form.component';
import { PaymentTypeDocumentListComponent } from './payment-type-document/payment-type-document-list/payment-type-document-list.component';
import { PaymentTypeDocumentListItemComponent } from './payment-type-document/payment-type-document-list/payment-type-document-list-item/payment-type-document-list-item.component';
import { CompanyStructureComponent } from './company-structure/company-structure.component';
import { CompanyStructureFormComponent } from './company-structure/company-structure-form/company-structure-form.component';
import { CompanyStructureListComponent } from './company-structure/company-structure-list/company-structure-list.component';
import { CompanyStructureListItemComponent } from './company-structure/company-structure-list/company-structure-list-item/company-structure-list-item.component';
import { BankAccountComponent } from './bank-account/bank-account.component';
import { BankAccountFormComponent } from './bank-account/bank-account-form/bank-account-form.component';
import { BankAccountListComponent } from './bank-account/bank-account-list/bank-account-list.component';
import { BankAccountListItemComponent } from './bank-account/bank-account-list/bank-account-list-item/bank-account-list-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DictionariesService } from './shared/dictionaries.service';


@NgModule({
  declarations: [
    DictionariesComponent, 
    ContractorsComponent, 
    ContractorsListComponent, 
    ContractorsFormComponent, 
    ContractorsListItemComponent, 
    PaymentTypeDocumentComponent, 
    ArticlesComponent, ArticlesFormComponent, 
    ArticlesListComponent, 
    ArticlesListItemComponent, 
    PaymentTypeDocumentFormComponent, 
    PaymentTypeDocumentListComponent, 
    PaymentTypeDocumentListItemComponent, 
    CompanyStructureComponent, 
    CompanyStructureFormComponent, 
    CompanyStructureListComponent,
    CompanyStructureListItemComponent,
    BankAccountComponent,
    BankAccountFormComponent,
    BankAccountListComponent,
    BankAccountListItemComponent
  ],
  imports: [
    CommonModule,
    DictionariesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  providers: [
    DictionariesService
  ]
})
export class DictionariesModule { }
