import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bank-account-list-item',
  templateUrl: './bank-account-list-item.component.html',
  styleUrls: ['./bank-account-list-item.component.scss']
})
export class BankAccountListItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
