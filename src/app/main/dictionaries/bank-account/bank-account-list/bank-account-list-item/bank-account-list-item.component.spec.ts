import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankAccountListItemComponent } from './bank-account-list-item.component';

describe('BankAccountListItemComponent', () => {
  let component: BankAccountListItemComponent;
  let fixture: ComponentFixture<BankAccountListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankAccountListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankAccountListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
