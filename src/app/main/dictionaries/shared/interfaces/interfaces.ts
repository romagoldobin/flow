export interface Contractor {
  name: string
  fullName: string
  TIN: number
  IEC: number
}

export interface ContractorShort {
  id: any
  name: string
}
