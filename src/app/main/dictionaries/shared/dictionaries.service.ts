import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Contractor } from './interfaces/interfaces';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class DictionariesService {
  constructor(private http: HttpClient
  ) {

  }

  /**
   * Создание контрагента в firebase
   * @param contractor модель контрагента
   */
  createContractor(contractor: Contractor) {
    return this.http.post<Contractor>(`${environment.dbUrl}/contractors.json`, contractor)
  }

  /**
   * Запрос контрагентов из бд firebase, маппин в короткую версию(в итоге получаем только имя и id)
   */
  getContractors(): Observable<any> {
    return this.http.get<Contractor[]>(`${environment.dbUrl}/contractors.json`)
    .pipe(
      map((response: any) => {
        return Object.keys(response).map( key =>(
          {
            id: key,
            name: response[key].name
          }
        ))
      })
    )
  }

  /**
   * Получение данных конрагента по идентификатору
   * @param id контрагента
   */
  getContractorById(id: string) {
    return this.http.get(`${environment.dbUrl}/contractors/${id}.json`)
  }

  removeContractor(id: string) {
    return this.http.delete<void>(`${environment.dbUrl}/contractors/${id}.json`)
  }
}
