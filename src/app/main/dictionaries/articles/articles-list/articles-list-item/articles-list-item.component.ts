import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-articles-list-item',
  templateUrl: './articles-list-item.component.html',
  styleUrls: ['./articles-list-item.component.scss']
})
export class ArticlesListItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
