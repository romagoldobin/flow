import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DictionariesComponent } from './dictionaries.component';
import { ContractorsComponent } from './contractors/contractors.component';
import { PaymentTypeDocumentComponent } from './payment-type-document/payment-type-document.component';
import { ArticlesComponent } from './articles/articles.component';
import { CompanyStructureComponent } from './company-structure/company-structure.component';
import { BankAccountComponent } from './bank-account/bank-account.component';

const routes: Routes = [{ path: '', component: DictionariesComponent, children: [
    {path: '', redirectTo:'contractors', pathMatch: 'full'},
    {path: 'contractors', component: ContractorsComponent},
    {path: 'payment-type-document', component: PaymentTypeDocumentComponent},
    {path: 'articles', component: ArticlesComponent},
    {path: 'company-structure', component: CompanyStructureComponent},
    {path: 'bank-account', component: BankAccountComponent}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DictionariesRoutingModule { }
