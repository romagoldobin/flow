import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-type-document-list-item',
  templateUrl: './payment-type-document-list-item.component.html',
  styleUrls: ['./payment-type-document-list-item.component.scss']
})
export class PaymentTypeDocumentListItemComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
