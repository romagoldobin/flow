import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { OrdersComponent } from './orders/orders.component';
import { AuthService } from '../logon/shared/services/auth.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [MainComponent, NavbarComponent, OrdersComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  providers:[AuthService]
})
export class MainModule { }
