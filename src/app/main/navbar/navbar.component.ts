import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/logon/shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isShow: Boolean;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  openMenu(event: Event) {
    event.preventDefault();
    this.isShow = !this.isShow;
  }

  logout() {
    event.preventDefault();
    this.authService.logout();
    this.router.navigate(['logon', 'signIn']);
  }

}
