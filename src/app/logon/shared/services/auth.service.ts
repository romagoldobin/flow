import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { User, authResponse } from '../interfaces';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators'

@Injectable({providedIn:'root'})
export class AuthService {
  constructor( private http: HttpClient ) {

  }

  get token():string {
    const expDate = new Date(localStorage.getItem('token-exprise'))
    if(new Date() > expDate) {
      this.logout()
      return null
    }
    return localStorage.getItem('token')
  }

  login(user: User): Observable<any> {
    user.returnSecureToken = true
    return this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`, user)
    .pipe(
      tap(this.setToken)
    )
  }

  logout() {
    this.setToken(null)
  }

  isAutentificated(): boolean {
    return !!this.token
  }

  private setToken(response: authResponse | null) {
    if(response) {
      // expriseDate указывает на то, когда кончится токен(текущее время + время жизни токена)
      const expiresDate = new Date(new Date().getTime() + +response.expiresIn * 1000)
      localStorage.setItem('token', response.idToken)
      localStorage.setItem('token-exprise', expiresDate.toString())
      console.log(expiresDate, response)
    } else {
      localStorage.clear();
    }

  }

}
