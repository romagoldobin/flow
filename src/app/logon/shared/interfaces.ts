export interface User {
  email: string
  password: string
  returnSecureToken?: boolean
}


export interface authResponse {
  idToken: string
  expiresIn: string
}
