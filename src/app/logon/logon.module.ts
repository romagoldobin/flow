import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'


import { LogonRoutingModule } from './logon-routing.module';
import { LogonComponent } from './logon.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthService } from './shared/services/auth.service';


@NgModule({
  declarations: [LogonComponent, SignInComponent, SignUpComponent],
  imports: [
    CommonModule,
    LogonRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[
    HttpClientModule
  ],
  providers: [AuthService]
})
export class LogonModule { }
