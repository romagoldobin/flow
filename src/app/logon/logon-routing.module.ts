import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogonComponent } from './logon.component';
import { SignInComponent } from './sign-in/sign-in.component';

const routes: Routes = [{ path: '', component: LogonComponent, children: [
  {path:'', redirectTo: 'signIn', pathMatch: 'full'},
  {path: 'signIn', component: SignInComponent}
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogonRoutingModule { }
